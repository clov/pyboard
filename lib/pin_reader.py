import uasyncio as asyncio
import pyb
from lib import units

class PinReader:
    """Pin reader with debouncing.

    :param pin: Pin whose value to read
    :param debouncing_time: Debouncing time
    """

    def __init__(
        self,
        pin: pyb.Pin,
        debouncing_time: units.Timedelta = units.Timedelta(ms=50),
    ) -> None:

        self.pin = pin
        self.debouncing_time = debouncing_time

    # abstractmethod
    async def main(self, handler) -> None:
        """Run the debouncer."""
        raise NotImplementedError

class FastPinReader(PinReader):
    """Pin reader with debouncing that reacts immediately to the press.

    :param pin: Pin whose value to read
    :param debouncing_time: Debouncing time in ms
    :param read_period: How often to read in case of no input

    Ref: https://www.reddit.com/r/embedded/comments/gf74p8/reliable_user_input_with_unreliable_physical/fprrygg
    """

    def __init__(
        self,
        pin: pyb.Pin,
        debouncing_time: units.Timedelta = units.Timedelta(ms=50),
        poll_period: units.Timedelta = units.Timedelta(ms=10),
    ) -> None:

        super().__init__(pin, debouncing_time)
        self.read_period = poll_period

    async def main(self, handler) -> None:
        while True:
            if self.pin.value() == 1:
                handler()
                await asyncio.sleep_ms(self.debouncing_time.to_milliseconds())
            else:
                await asyncio.sleep_ms(self.read_period.to_milliseconds())

if __name__ == '__main__':
    pin = pyb.Pin(pyb.Pin.board.X1, pyb.Pin.IN, pyb.Pin.PULL_DOWN)
    pin_reader = FastPinReader(pin)
    handler = lambda: print('Button pressed')
    asyncio.run(pin_reader.main(handler))
