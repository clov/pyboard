import uasyncio as asyncio
import pyb
from lib import (
    FastPinReader,
    PinReader,
    utils,
)

def pass_fun(n: int) -> None:
    pass

class TwoPinCounter:
    """A counter controlled by two pins, other increasing and the other
    decreasing the counter.

    :param increase_pin_reader: Pin reader for increasing the counter
    :param decrease_pin_reader: Pin reader for decreasing the counter
    :param bounds: Interval where the counter is saturated
    :param counter_init: Initial value for the counter
    :param step: Step size for the counter
    """

    def __init__(
        self,
        increase_pin_reader: PinReader,
        decrease_pin_reader: PinReader,
        bounds: tuple[float, float] = (-float('inf'), float('inf')),
        counter_init: int = 0,
        step: int = 1,
        handler = pass_fun,
    ) -> None:

        self.increase_pin_reader = increase_pin_reader
        self.decrease_pin_reader = decrease_pin_reader
        self.bounds = bounds
        self._counter = counter_init
        self.step = step
        self.handler = handler

        self._increase_task: asyncio.Task
        self._decrease_task: asyncio.Task

    @property
    def counter(self) -> int:
        return self._counter
    
    @counter.setter
    def counter(self, n: int) -> None:
        self._counter = round(utils.saturate(n, self.bounds))
    
    def increase_handler(self) -> None:
        self.counter += self.step
        self.handler(self.counter)
    
    def decrease_handler(self) -> None:
        self.counter -= self.step
        self.handler(self.counter)
    
    async def main(self) -> None:
        self._increase_task = asyncio.create_task(
            self.increase_pin_reader.main(self.increase_handler),
        )
        self._decrease_task = asyncio.create_task(
            self.decrease_pin_reader.main(self.decrease_handler),
        )
        await asyncio.gather(self._increase_task, self._decrease_task)
    
    def cleanup(self) -> None:
        pass
    
if __name__ == '__main__':

    print('starting')

    def handler(n: int) -> None:
        print('counter updated to', n)

    increase_pin = pyb.Pin(pyb.Pin.board.X1, pyb.Pin.IN, pyb.Pin.PULL_DOWN)
    decrease_pin = pyb.Pin(pyb.Pin.board.X2, pyb.Pin.IN, pyb.Pin.PULL_DOWN)
    counter = TwoPinCounter(
        FastPinReader(increase_pin),
        FastPinReader(decrease_pin),
        (0., float('inf')),
        handler=handler,
    )
    asyncio.run(counter.main())
