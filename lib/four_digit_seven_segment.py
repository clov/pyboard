import uasyncio as asyncio
import pyb
import utime as time
from lib import units

class Segments:
    """Segments of one position. Value of each argument represents the pin the
    segment is connected to.

    Naming of segments:

       _t_
    tl|_m_|tr
    bl|___|br .d
        b 
    
    :param t:  top
    :param tl: top left
    :param tr: top right
    :param b:  bottom
    :param bl: bottom left
    :param br: bottom right
    :param m:  middle
    :param d:  dot
    """

    def __init__(
        self,
        t : pyb.Pin = pyb.Pin(pyb.Pin.board.Y8),
        tl: pyb.Pin = pyb.Pin(pyb.Pin.board.Y9),
        tr: pyb.Pin = pyb.Pin(pyb.Pin.board.Y12),
        b : pyb.Pin = pyb.Pin(pyb.Pin.board.Y2),
        bl: pyb.Pin = pyb.Pin(pyb.Pin.board.Y1),
        br: pyb.Pin = pyb.Pin(pyb.Pin.board.Y4),
        m : pyb.Pin = pyb.Pin(pyb.Pin.board.Y5),
        d : pyb.Pin = pyb.Pin(pyb.Pin.board.Y3),
    ) -> None:

        self.t = t
        self.tl = tl
        self.tr = tr
        self.b = b
        self.bl = bl
        self.br = br
        self.m = m
        self.d = d

        self.all = [t, tr, br, b, bl, tl, m, d]
        for pin in self.all:
            pin.init(pyb.Pin.OUT_PP)

        self.figures_to_pins = {
            None: [],
            0: [t, tl, bl, b, br, tr],
            1: [tr, br],
            2: [t, tr, m, bl, b],
            3: [t, tr, m, br, b],
            4: [tl, m, tr, br],
            5: [t, tl, m, br, b],
            6: [t, tl, bl, b, br, m],
            7: [t, tr, br],
            8: [t, tr, br, b, bl, tl, m],
            9: [m, tl, t, tr, br, b],
        }

    def pins_from_figure(self, x: int | None) -> list[pyb.Pin]:
        """Segments/pins of a figure `x`."""
        return self.figures_to_pins[x]

class Positions:
    """The four different positions. Value of each argument represents the pin
    the position is connected to.

    :param p0: position 0
    :param p1: position 1
    :param p2: position 2
    :param p3: position 3
    """

    def __init__(
        self,
        p0: pyb.Pin = pyb.Pin(pyb.Pin.board.Y7),
        p1: pyb.Pin = pyb.Pin(pyb.Pin.board.Y10),
        p2: pyb.Pin = pyb.Pin(pyb.Pin.board.Y11),
        p3: pyb.Pin = pyb.Pin(pyb.Pin.board.Y6),
    ) -> None:
        
        self.p0 = p0
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3

        self.all = [p0, p1, p2, p3]
        for pin in self.all:
            pin.init(pyb.Pin.OUT_PP)

    def pin_from_position(self, pos: int) -> pyb.Pin:
        """Pin of position `pos`."""
        return self.all[pos]

class FourDigitSevenSegment:
    """Controller for a 4-digit 7-segment display.

    :param position_sleep: Sleep time between subsequent positions
    """

    def __init__(
        self,
        position_sleep: units.Timedelta,
        segments: Segments = Segments(),
        positions: Positions = Positions(),
    ) -> None:

        self.position_sleep = position_sleep
        self.segments = segments
        self.positions = positions

        self._digits: tuple[
            int | None, int | None, int | None, int | None,
        ] = (None, None, None, None)
        self._number: int | None
        self.set_number(None)

        self._current_position: int = 0
        self._turn_on_task: asyncio.Task | None = None

    def get_digits(
        self,
    ) -> tuple[int | None, int | None, int | None, int | None]:
        return self._digits

    def set_digits(self, number: int | None) -> None:
        if number is None:
            self._digits = (None, None, None, None)
            return
        self._digits = (
            (number//1000) % 10,
            (number//100) % 10,
            (number//10) % 10,
            number % 10,
        )

    def get_number(self) -> int | None:
        return self._number
    
    def set_number(self, number: int | None) -> None:
        self._number = number
        self.set_digits(number)

    def _next_position(self) -> None:
        """Advance to next position."""
        self._current_position = (self._current_position + 1) % 4
    
    def _pins_off(self, pins: list[pyb.Pin]) -> None:
        for pin in pins:
            pin.off()

    def _pins_on(self, pins: list[pyb.Pin]) -> None:
        for pin in pins:
            pin.on()

    def _segments_off(self) -> None:
        self._pins_off(self.segments.all)

    def _choose_position(self, n: int) -> None:
        # turn every position OFF (pin ON)
        self._pins_on(self.positions.all)
        # turn position `n` ON (pin OFF)
        self.positions.pin_from_position(n).off()

    def _write_figure(self, x: int | None) -> None:
        """Write figure `x` to current position."""
        self._segments_off()
        self._pins_on(self.segments.pins_from_figure(x))

    def _write_figure_and_advance(self) -> None:
        """Write figure and advance to next position."""
        digits = self.get_digits()
        self._write_figure(digits[self._current_position])
        self._choose_position(self._current_position)
        self._next_position()

    def turn_off(self) -> None:
        """Turn the display off."""
        self._segments_off()

    async def turn_on(
        self,
        ontime: units.Timedelta | None = None,
    ) -> None:
        """Turn the display on for `ontime` milliseconds."""

        ontime_start = time.ticks_ms()
        while True:
            if (
                ontime is not None and
                time.ticks_ms() - ontime_start >= ontime.to_milliseconds()
            ):
                self.turn_off()
                break
            self._write_figure_and_advance()
            await asyncio.sleep_ms(self.position_sleep.to_milliseconds())

    def write(
        self,
        number: int | None,
        ontime: units.Timedelta | None = None,
    ) -> None:
        """Write `number` to display and show it for `ontime` milliseconds."""

        self.set_number(number)
        # If there is already a `turn_on_task` running, cancel it before
        # running a new one to avoid them running concurrently. Thus a new
        # write immediately overrides the previous one, and doesn't wait for it
        # to complete.
        if self._turn_on_task is not None:
            self._turn_on_task.cancel()
        self._turn_on_task = asyncio.create_task(self.turn_on(ontime))
    
    def cleanup(self) -> None:
        self.turn_off()

if __name__ == '__main__':
    display = FourDigitSevenSegment(units.Timedelta(ms=5)) 
    asyncio.run(display.write(57, ontime=units.Timedelta(s=3)))
