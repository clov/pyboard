from .utils import DataClass

SECOND = 1
MILLISECOND = 1e-3 * SECOND
MICROSECOND = 1e-6 * SECOND
MINUTE = 60 * SECOND
HOUR = 60 * MINUTE
DAY = 24 * HOUR

STR_TOL = 1e-9

class Timedelta(DataClass):
    """Time difference."""

    def __init__(
        self,
        d: float = 0.,
        h: float = 0.,
        min: float = 0.,
        s: float = 0.,
        ms: float = 0.,
        us: float = 0.,
    ) -> None:

        super().__init__()
        self.d = d
        self.h = h
        self.min = min
        self.s = s
        self.ms = ms
        self.us = us
    
    # overrides
    def to_json_dict(self) -> dict:
        return self.__dict__
    
    @classmethod
    def from_json_dict(cls, data: dict) -> Timedelta:
        return Timedelta(**data)
    
    def _to_seconds(self) -> float:
        return (
            self.d * DAY
            + self.h * HOUR
            + self.min * MINUTE
            + self.s * SECOND
            + self.ms * MILLISECOND
            + self.us * MICROSECOND
        )
    
    def to_seconds(self) -> int:
        return round(self._to_seconds())
    
    def to_milliseconds(self) -> int:
        return round(self._to_seconds() / MILLISECOND)
    
    # overrides
    def __repr__(self) -> str:
        res = ''
        if abs(self.d) > STR_TOL:
            res += str(self.d) + ' d '
        if abs(self.h) > STR_TOL:
            res += str(self.h) + 'h '
        if abs(self.min) > STR_TOL:
            res += str(self.min) + ' min '
        if abs(self.s) > STR_TOL:
            res += str(self.s) + ' s '
        if abs(self.ms) > STR_TOL:
            res += str(self.ms) + ' ms '
        if abs(self.ms) > STR_TOL:
            res += str(self.us) + ' us '
        res = res.strip()
        return self.__class__.__name__ + '(' + res + ')'
    