class IntEnum:
	"""Simple replacement for Python's `enum.IntEnum` without metaclasses."""

	@classmethod
	def getitem(cls, key: str) -> int:
		return cls.__dict__[key]

class DataClass:
	"""Simple replacement for Python's `dataclasses.dataclass` in a class form.
	"""

	def to_json_dict(self) -> dict:
		res = {}
		for key, value in self.__dict__.items():
			if isinstance(value, DataClass):
				json_value = value.to_json_dict()
			else:
				json_value = value
			res[key] = json_value
		return res

	def __repr__(self) -> str:
		return self.__class__.__name__ + '(' +  str(self.__dict__) + ')'

def saturate(x: float, bounds: tuple[float, float]) -> float:
	a, b = bounds
	return min(max(x, a), b)

def pass_fun() -> None:
    pass
