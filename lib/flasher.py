import uasyncio as asyncio
import pyb
from lib import units

class Flasher:
	"""A controller for keeping a pin ON for a given time, then OFF for a
	given time, and repeating this.

	:param pin: Pin to control
	:param ontime: How long to keep ON
	:param offtime: How long to keep OFF
	:param unit: Time unit to determine which sleep function to use, and thus
	    to determine the unit of `ontime` and `offtime`
	"""

	def __init__(
		self,
		pin: pyb.Pin,
		ontime: units.Timedelta,
		offtime: units.Timedelta,
		debug: bool = False,
	) -> None:

		self.pin = pin
		self.ontime = ontime
		self.offtime = offtime
		self.debug = debug

		pin.init(pyb.Pin.OUT_PP)
	
	async def main(self) -> None:
		while True:
			if self.debug:
				print('turning pin HIGH')
			self.pin.high()
			await asyncio.sleep(self.ontime.to_seconds())
			if self.debug:
				print ('turning pin LOW')
			self.pin.low()
			await asyncio.sleep(self.offtime.to_seconds())
	
	def cleanup(self) -> None:
		self.pin.low()

if __name__ == '__main__':
	ledpin = pyb.Pin(pyb.Pin.board.X5)
	flasher = Flasher(
		ledpin, units.Timedelta(s=1), units.Timedelta(s=3), debug=True,
	)
	asyncio.run(flasher.main())
