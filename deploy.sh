#!/bin/bash

project=$1
board=/media/clov/PYBFLASH

echo "syncing lib ..."
rsync --exclude lib/__pycache__  -azv lib $board

echo
echo "syncing project ..."
if [[ -n $project ]]
    then rsync -azv $project $board/main.py
fi