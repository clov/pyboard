"""Adjustable timer for usage in an aeroponics sprayer.

Turns the sprayer ON for some time, turns it OFF for some time, and repeats.
The ON and OFF times can be adjusted by 4 buttons: 2 buttons for ON time 
(other increasing and other decreasing) and 2 buttons for OFF time.
The current ON/OFF time is shown on display when an adjustment is made.
"""

import uasyncio as asyncio
import json
import pyb
from lib import (
    FastPinReader,
    Flasher,
    FourDigitSevenSegment,
    TwoPinCounter,
    units,
    utils,
)

print('starting')

class Config(utils.DataClass):

    def __init__(
        self,
        ontime: units.Timedelta = units.Timedelta(s=10),
        offtime: units.Timedelta = units.Timedelta(min=15),
    ) -> None:

        self.ontime = ontime
        self.offtime = offtime
    
    @classmethod
    def from_json_dict(cls, data: dict) -> Config:
        return Config(
            ontime = units.Timedelta.from_json_dict(data['ontime']),
            offtime = units.Timedelta.from_json_dict(data['offtime']),
        )
    
    @classmethod
    def load(cls, filename: str) -> Config:
        try:
            with open(filename, 'r') as f:
                return Config.from_json_dict(json.load(f))
        except OSError:
            return Config()

    def save(self, filename: str) -> None:
        with open(filename, 'w') as f:
            json.dump(self.to_json_dict(), f)

config_filename = 'config.json'
config = Config.load(config_filename)
print('loaded', str(config))

display = FourDigitSevenSegment(units.Timedelta(ms=5))
flasher = Flasher(
    pyb.Pin.board.X9, config.ontime, config.offtime, debug=True,
)
# Was `ontime` or `offtime` updated after previous config save?
update_event = asyncio.Event()

def ontime_counter_handler(n: int) -> None:
    display.write(n, units.Timedelta(s=3))
    ontime = units.Timedelta(s=n)
    print('ontime updated to', ontime)
    flasher.ontime = ontime
    config.ontime = ontime
    update_event.set()

def offtime_counter_handler(n: int) -> None:
    display.write(n, units.Timedelta(s=3))
    offtime = units.Timedelta(s=n)
    print('offtime updated to', offtime)
    flasher.offtime = offtime
    config.offtime = offtime
    update_event.set()

mode = pyb.Pin.IN
pull = pyb.Pin.PULL_DOWN
debouncing_time = units.Timedelta(ms=100)
ontime_counter = TwoPinCounter(
    FastPinReader(pyb.Pin(pyb.Pin.board.X1, mode, pull), debouncing_time),
    FastPinReader(pyb.Pin(pyb.Pin.board.X2, mode, pull), debouncing_time),
    (1., float('inf')),
    config.ontime.to_seconds(),
    handler=ontime_counter_handler,
)
offtime_counter = TwoPinCounter(
    FastPinReader(pyb.Pin(pyb.Pin.board.X3, mode, pull), debouncing_time),
    FastPinReader(pyb.Pin(pyb.Pin.board.X4, mode, pull), debouncing_time),
    (1., float('inf')),
    config.offtime.to_seconds(),
    handler=offtime_counter_handler,
)

async def config_saver() -> None:
    while True:
        await update_event.wait()
        print('update_event was set')
        await asyncio.sleep(30)
        print('saving', str(config)) 
        config.save(config_filename)
        update_event.clear()

async def main() -> None:
    tasks = [
        asyncio.create_task(flasher.main()),
        asyncio.create_task(ontime_counter.main()),
        asyncio.create_task(offtime_counter.main()),
        asyncio.create_task(config_saver()),
    ]
    await asyncio.gather(*tasks)

def cleanup() -> None:
    display.cleanup()
    flasher.cleanup()
    ontime_counter.cleanup()
    offtime_counter.cleanup()

try:
    asyncio.run(main())
except KeyboardInterrupt:
    cleanup()
    raise
