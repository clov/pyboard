# Pyboard projects


## Description
Personal projects to familiarize myself to electronics and Pyboard programming.

## Installation/usage
Copy (needed) files to Pyboard, eject media, and press reset on Pyboard on
start the program. You can also run the program in REPL through
`screen /dev/ttyACM0`, or `mpremote` (possibly with `sudo`).

`deploy.sh` is a utility script to copy the files to Pyboard. For example, to
deploy `projects/aeroponics_timer/main.py` as the main program, use
`./deploy.sh projects/aeroponics_timer/main.py`. `lib` is also always copied.

Tested on MicroPython 1.20

## License
GNU General Public License version 3

